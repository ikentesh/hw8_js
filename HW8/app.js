"use strict";

/*Теоретичні питання

Опишіть своїми словами що таке Document Object Model (DOM) 

DOM - це об'єктна модель документа, в рамках цієї моделі вміст документа подається у вигляді різних об'єктів, які перебувають у певних відносинах. 
Кожен вкладений елемент HTML сприймається як дочірній об'єкт стосовно того, що його оточує. Атрибути HTML перетворюються на властивості таких об'єктів. 
Іншими словами, це подання HTML-документа у вигляді дерева тегів. Таке дерево потрібне для правильного відображення сайту та внесення змін на сторінках 
за допомогою JavaScript. Основна мета DOM – дозволити веб-програмісту створювати скрипти для динамічного доступу до веб-сторінки та оновлення елементів 
її вмісту, структури та стилів.


Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerText - показує весь текстовий вміст, який не належить до синтаксису HTML. Тобто будь-який текст, розміщений між тегами елемента, буде записаний в 
innerText. Причому якщо всередині innerText будуть ще якісь елементи HTML зі своїм вмістом, він проігнорує самі елементи і поверне їх внутрішній текст.

innerHTML – покаже текстову інформацію рівно по одному елементу. При виведенні побачимо і текст, і розмітку HTML-документа, яка може бути розміщена
між тегами основного елемента.


Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Вибір елементів здебільшого виконується за допомогою цих методів:
querySelector;
querySelectorAll.
Вони дозволяють виконати пошук HTML-елементів за CSS-селектором. У цьому querySelector вибирає один елемент, а querySelectorAll – усі.

Крім них є ще:
getElementById - отримує один елемент по id;
getElementsByClassName - дозволяє знайти всі елементи із зазначеним класом або класами;
getElementsByTagName - вибирає елементи за тегом;
getElementsByName - отримує всі елементи із зазначеним значенням атрибута name.*/

// Завдання

const paragraph = Array.from(document.querySelectorAll("p"));
paragraph.forEach((element) => {
  element.style.background = "#ff0000";
});

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.children);
console.log(optionsList.childNodes);

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = "This is a paragraph";

const mainHeader = document.querySelector(".main-header");
[...mainHeader.children].forEach((el) => {
  el.className = "nav-item";
});
console.log(mainHeader.children);

const sectionTitle = document.querySelectorAll(".section-title");
[...sectionTitle].forEach((el) => {
  el.classList.toggle("section-title");
});
console.log(sectionTitle);
